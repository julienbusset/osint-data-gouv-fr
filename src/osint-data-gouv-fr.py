# Import des bibliothèques standards
import json
import requests
import magic
import chardet
import argparse

# Import des bibliothèques spécifiques
from lib import rechercher

# Le catalogue est lui-même un dataset, que l'on peut consulter avec cette autre URL, mais qui donne beaucoup plus de résultats, et je n'ai pas vérifié s'il est plus pertinent d'utiliser cela plutôt que le endpoint 'catalog'
# CATALOGUE_DONNEES = "https://www.data.gouv.fr/api/1/datasets/r/f868cca6-8da1-4369-a78d-47463f19a9a3"
CATALOGUE_DONNEES = "https://www.data.gouv.fr/api/1/site/catalog.json"

parser = argparse.ArgumentParser()
parser.add_argument('-q', help='Mot ou expression à rechercher dans data.gouv.fr. Pour une expression, penser à la mettre entre quotes.')
a_chercher = parser.parse_args().q
print("Mot ou expression à rechercher : " + a_chercher)

print("Téléchargement du catalogue de jeux de données")
catalog = requests.get(CATALOGUE_DONNEES)
decoded_catalog = catalog.content.decode('utf-8')

#pour le catalogue en json
catalog_list = json.loads(decoded_catalog)["@graph"]

print("Catalogue téléchargé, " + str(len(catalog_list)) + " jeux de données dans lesquels rechercher.\n")
output_file = open("out/resultats.txt", "w")

occurrences_trouvees = 0
# 3 listes pour les URL qui posent des problèmes
octet_stream_list = []
ssl_error_list = []
encoding_problem_list = []
mime = magic.Magic(mime=True)

# pour débugger un jeux de données :
# print(catalog_list[103]["accessURL"])

# Pour démarrer le catalogue à un certain item (utile pour le débuggage), on fait comme ceci :
# for catalog_number, catalog_item in enumerate(catalog_list[102:]):
for catalog_number, catalog_item in enumerate(catalog_list):
    print ("\033[F" + str(catalog_number) + " jeu(x) de données traité(s), " + str(occurrences_trouvees) + " occurrence(s) trouvée(s)")
    if "accessURL" in catalog_item:
        try:
            dataset_url = catalog_item["accessURL"]
            dataset = requests.get(dataset_url, stream=True).content
            if mime.from_buffer(dataset) == 'application/zip':
                occurrences_trouvees = rechercher.dans_zip(dataset, dataset_url, catalog_item, output_file, occurrences_trouvees, a_chercher, encoding_problem_list)
            elif mime.from_buffer(dataset) == 'application/octet-stream':
                octet_stream_list.append(dataset_url)
            else:
                occurrences_trouvees = rechercher.dans_fichier_quelconque(dataset, dataset_url, catalog_item, output_file, occurrences_trouvees, a_chercher, encoding_problem_list)
        except requests.exceptions.SSLError:
            ssl_error_list.append(dataset_url)
            continue
output_file.close()
print("\033[F" + str(len(catalog_list)))
print("Liste des " + str(len(octet_stream_list)) + " URL qui ont renvoyées un application/octet-stream (non traitées) :")
print(octet_stream_list)
print("Liste des " + str(len(ssl_error_list)) + " URL qui ont renvoyées une erreur SSL :")
print(ssl_error_list)
print("Liste des " + str(len(encoding_problem_list)) + "URL qui ont renvoyées un ou des fichiers pour lesquels l'encodage n'a pas été détecté correctement")
print(encoding_problem_list)
print("Traitement terminé")

def test(variable):
    print("test" + str(variable))    