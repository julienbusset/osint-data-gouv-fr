import zipfile
import shutil
import io
import chardet
from os import path, walk

def dans_zip(dataset, dataset_url, catalog_item, output_file, occurrences_trouvees, a_chercher, encoding_problem_list):
    zipped_dataset = io.BytesIO(dataset)
    with zipfile.ZipFile(zipped_dataset, 'r') as z:
        unzipped_dataset = z.extractall(path='tmp')
    # reste à faire la recherche dans tous les fichiers décompressés…
    for root, dir_names, file_names in walk('tmp'):
        for file_name in file_names:
            fullpath = path.join(root, file_name)
            data_to_explore = open(fullpath, "rb").read()
            encoding = chardet.detect(data_to_explore)#.get("encoding")
            # Sur certains fichiers, la confiance est à seulement 0.2…, et l'encodage n'est pas déterminé correctement.
            # Tant qu'on ne trouve pas pourquoi, on ajoute la condition sur la confiance (>0.8)
            if encoding is not None and encoding.get("confidence") > 0.8 :
                decoded_data_to_explore = data_to_explore.decode(encoding.get("encoding"))
                flag_trouve = False
                for line_number, line in enumerate(decoded_data_to_explore.split("\n")):
                    if a_chercher.casefold() in line.casefold():
                        if not flag_trouve:
                            output_file.write("Trouvé à l'adresse " + dataset_url)
                            if "title" in catalog_item:
                                output_file.write(" (intitulé \"" + catalog_item["title"] + "\")")
                            output_file.write("\r\n dans le fichier nommé " + file_name + " de l'archive téléchargée à cette adresse")
                            output_file.write(" : \r\n")
                            flag_trouve = True
                        output_file.write("  " + str(line_number) + ": " + line + "\r\n")
                        occurrences_trouvees += 1
            else:
                encoding_problem_list.append(dataset_url)
                continue
    # on fait le ménage de tout ce qu'on a décompressé
    shutil.rmtree(path='tmp')
    return occurrences_trouvees

def dans_fichier_quelconque(dataset, dataset_url, catalog_item, output_file, occurrences_trouvees, a_chercher, encoding_problem_list):
    flag_trouve = False
    charset = chardet.detect(dataset)["encoding"]
    if charset is not None:
        decoded_dataset = dataset.decode(charset)
        for line_number, line in enumerate(decoded_dataset.split("\n")):
            if a_chercher.casefold() in line.casefold():
                if not flag_trouve:
                    to_print = "Trouvé à l'adresse " + dataset_url
                    if "title" in catalog_item:
                        to_print += " (intitulé \"" + catalog_item["title"] + "\")"
                    output_file.write(to_print + " : \r\n")
                    flag_trouve = True
                output_file.write("  " + str(line_number) + ": " + line + "\r\n")
                occurrences_trouvees += 1
    else:
        encoding_problem_list.append(dataset_url)
    return occurrences_trouvees